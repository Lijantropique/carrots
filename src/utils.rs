use std::io::{self,Write};
use std::{thread, time};
pub struct Box {
    pub colour: String,
    pub carrot: bool,
    pub owner: String,
}

impl Box {
    pub fn info(&self) {
        if self.carrot{
            println!("Congrats {}, your {} box have the carrot!", self.owner,self.colour);
        } else {
            println!("Sorry {}, your {} box doesn't have the carrot!", self.owner,self.colour);
        }
    }
}
pub enum Options {
    Hidden,
    Red,
    Both,
}

pub const BOXES:&str ="  _________       _________
 /        /|     /        /|
+--------+ |    +--------+ | 
|  RED   | |    |  GOLD  | | 
|  BOX   | /    |  BOX   | /
+--------+/     +--------+/\n";

pub const RED_EMPTY:&str ="  __________ 
  |        |
  |        |
  |________|      _________
 /        /|     /        /|
+--------+ |    +--------+ | 
|  RED   | |    |  GOLD  | | 
|  BOX   | /    |  BOX   | /
+--------+/     +--------+/\n";


pub const RED_CARROT:&str ="  __________
  |        |
  |   🥕   |
  |________|      _________
 /        /|     /        /|
+--------+ |    +--------+ | 
|  RED   | |    |  GOLD  | | 
|  BOX   | /    |  BOX   | /
+--------+/     +--------+/\n";

pub const RED_EMPTY_BOXES:&str ="  __________      __________
  |        |      |        |
  |        |      |   🥕   |
  |________|      |________|
 /        /|     /        /|
+--------+ |    +-----v--+ | 
|  RED   | |    |  GOLD  | | 
|  BOX   | /    |  BOX   | /
+--------+/     +--------+/\n";

pub const RED_CARROT_BOXES:&str ="  __________      __________
  |        |      |        |
  |   🥕   |      |        |
  |________|      |________|
 /        /|     /        /|
+--------+ |    +--------+ | 
|  RED   | |    |  GOLD  | | 
|  BOX   | /    |  BOX   | /
+--------+/     +--------+/\n";

pub fn banner(message: &str){
    println!("----------------------
{message}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n");
}

pub fn get_input(message: &str) -> String {
    print!("{message} ");
    io::stdout().flush().unwrap();
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");
    guess.trim().to_string()
}

pub fn print_boxes(red:&Box, option:Options){
    match option {
        Options::Hidden => println!("{}", BOXES),
        Options::Red => {
            if red.carrot{
                println!("{}", RED_CARROT);
            } else{
                println!("{}", RED_EMPTY);
            }
        },
        Options::Both => {
            if red.carrot{
                println!("{}", RED_CARROT_BOXES);
            } else{
                println!("{}", RED_EMPTY_BOXES);
            }
        },
    }
}

pub fn print_carrots(){
    let line_width = 40;
    for idx in 1..83 {
        let n = (idx+4*idx)%line_width;
        let mut tmp = " ".repeat(n);
        tmp += &"... 🥕 ...";
        println!("{tmp}");
        thread::sleep( time::Duration::from_millis(100));
    }
    println!("\n ==================== 🥕 ==================== \n");
}

pub fn swap_boxes(red:&mut Box, golden:&mut Box){
    let tmp = red.carrot;
    red.carrot = golden.carrot;
    golden.carrot = tmp;
}