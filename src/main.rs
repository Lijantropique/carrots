mod utils;
use rand;
use std::{thread, time};

fn main() {
    println!("Hello, world!");
    utils::banner("🥕 Carrots 🥕");
    let human1 = utils::get_input("Human player 1, enter your name:");
    let human2 = utils::get_input("Human player 2, enter your name:");

    let mut red = utils::Box{
        colour: String::from("red"),
        carrot: rand::random(),
        owner: human1,
    };

    let mut golden = utils::Box{
        colour: String::from("golden"),
        carrot: !red.carrot,
        owner: human2,
    };

    utils::print_boxes(&red, utils::Options::Hidden);
    println!("{}, you have a RED box in front of you.", red.owner); 
    println!("{}, you have a GOLD box in front of you.",golden.owner);
    let _ = utils::get_input("Press Enter to continue...");
    let _ = utils::get_input(&format!("\nWhen {} has closed their eyes, press Enter...", golden.owner));
    println!("{} here is the inside of your box:",red.owner );
    utils::print_boxes(&red, utils::Options::Red);
    let _ = utils::get_input("Press Enter to continue...");
    utils::print_carrots();
    utils::print_boxes(&red, utils::Options::Hidden);
    let swap = utils::get_input(&format!("{}, do you want to swap boxes? (y/n)", golden.owner));
    match swap.as_str(){
        "Yes"|"Y"|"y"  => {
            println!("ok, let's swap the box content ...");
            utils::swap_boxes(&mut red, &mut golden)
        },
        _ => println!("your call ... let's keep the box content as it is ... "),
    }
    thread::sleep( time::Duration::from_millis(2000));
    utils::print_boxes(&red, utils::Options::Both);
    golden.info();

    if red.carrot{
        println!("{} is the winner!", red.owner);
    } else {
        println!("{} is the winner!", golden.owner);
    }
}
